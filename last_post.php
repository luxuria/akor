<?php
// NE PAS SUPPRIMER CE FICHIER !! UTILISÉ PAR AGILE CRM
// DO NOT REMOVE THIS FILE !! USED BY AGILE CRM

// Redirect to the last blog post
try {
    if( empty($_GET['i']) ) {
        $post = 1;
    } elseif( intval($_GET['i']) < 1 ) {
        $post = 1;
    } else {
        $post = intval($_GET['i']);
    }
    $json = file_get_contents('http://transformons.akorconsulting.com/wp-json/wp/v2/posts?per_page='.$post);
    $posts = json_decode($json);
    $newURL = $posts[$post-1]->link;
    header('Location: '.$newURL);
} catch(Exception $e){
    $newURL = 'http://transformons.akorconsulting.com';
    header('Location: '.$newURL);
}
?>
